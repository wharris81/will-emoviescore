﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Xunit;

namespace eMovies.Tests.Extensions
{
    public static class AssertExtensions
    {
        public static void CollectionMatchesAll<T>(IEnumerable<T> expected, IEnumerable<T> actual, Func<T, T, bool> comparer)
        {
            foreach(var item in expected)
            {
                if (!actual.Any(a=> comparer(item, a)))
                {
                    throw new KeyNotFoundException($"Actual is missing: {JsonConvert.SerializeObject(item)}");
                }
            }

            Assert.True (expected.All(shouldItem => actual.Any(a => comparer(shouldItem, a))));
        }        

    }
}
