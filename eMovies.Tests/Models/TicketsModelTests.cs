﻿using System;
using System.Collections.Generic;
using System.Text;
using eMovies.Comparers;
using eMovies.Models;
using Xunit;

namespace eMovies.Tests.Models
{
    public class TicketsModelTests
    {
        [Fact]
        public void WhenMoviesIsEmpty_TotalIsCalculated_OutputIsCorrect()
        {
            //arrange
            var ticketsModel = new TicketsModel();

            //act

            //assert
            Assert.Equal(0, ticketsModel.NewTotal);

            Assert.Equal("£0.00", ticketsModel.CurrencyTotal);
        }


        [Fact]
        public void WhenMoviesExist_TotalIsCalculated_OutputIsCorrect()
        {
            var ticketsModel = new TicketsModel
            {
                Movies = new[] {
                    new Movie
                    {
                        Price = 2.99,
                        Quantity = 2
                    },
                    new Movie
                    {
                        Price = 4.67,
                        Quantity = 5
                    }
                }
            };
              
            Assert.Equal(29.33, ticketsModel.NewTotal, new DoubleComparer(2));
        }
    }
}
