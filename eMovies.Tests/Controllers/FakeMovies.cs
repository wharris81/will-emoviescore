﻿using eMovies.Models;

namespace eMovies.Tests.Controllers
{
    internal class FakeMovies
    {
        public Movie[] LoadMovies()
        {
            Movie[] FakeMovies = new Movie[2];

            FakeMovies[0] = new Movie
            {
                Id = 1,
                Name = "Movie1",
                Price = 1.11,
                Description = "Movie1Decsription",
                ImagePath = "Movie1IP",
                Quantity = 1
            };
            FakeMovies[1] = new Movie
            {
                Id = 2,
                Name = "Movie2",
                Price = 2.22,
                Description = "Movie2Decsription",
                ImagePath = "Movie2IP",
                Quantity = 2
            };

            return FakeMovies;
        }
    }    
}


