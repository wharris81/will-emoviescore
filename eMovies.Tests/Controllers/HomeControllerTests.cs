﻿using eMovies.Controllers;
using eMovies.Models;
using eMovies.Repositories;
using eMovies.Services;
using eMovies.Tests.Extensions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace eMovies.Tests.Controllers
{
    public class HomeControllerTests
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private Mock<IMovieRepository> _mockMovieRepository;
        private Mock<ISessionService> _mockSessionService;
        private HomeController _controller;

        public HomeControllerTests(ITestOutputHelper  testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;

            _mockMovieRepository = new Mock<IMovieRepository>();

            _mockSessionService = new Mock<ISessionService>();

            _controller = new HomeController(_mockSessionService.Object, _mockMovieRepository.Object);
        }
        
        [Fact]
        public void WhenHomepageIsLoaded_ValidMoviesAreReturned()
        {  
            _mockMovieRepository.Setup(a => a.LoadMovies())
                .Returns(new FakeMovies().LoadMovies());            
            
            var results = _controller.Index();

            var resultsModel = ((ViewResult)results).Model as TicketsModel;

            AssertExtensions.CollectionMatchesAll(new FakeMovies().LoadMovies(), resultsModel.Movies, (a, b) => a.Id == b.Id);
        }

        [Fact]
        public void WhenSessionIsLoaded_StringSet_TestPasses()
        {   
            _controller.SessionTest();          

            _mockSessionService.Verify(a => a.SetString("key", "value"), Times.Once);
        }

        [Fact]
        public void WhenSessionIsLoaded_ObjectSet_TestPasses()
        {
            _controller.SessionTest();

            _mockSessionService.Verify(a => a.SetObject("key", new[] { "a", "b" }), Times.Once);
        }

        [Fact]
        public void Index_OrderButtonStateValid_SetCustomerIDInSession()
        {
            _mockMovieRepository.Setup(a => a.NewCustomer(It.IsAny<TicketsModel>()))
                .Returns(10);

            var ticketsModel = BuildTicketsModel();
            ticketsModel.Movies[1].Quantity = 1;       
            
            var results = _controller.Index(ticketsModel, "Order");

            _mockSessionService.Verify(a => a.SetObject("customerId", 10), Times.Once);
        }

        [Fact]
        public void Index_OrderButtonStateValid_RedirectOrder()
        {
            var ticketsModel = BuildTicketsModel();
            ticketsModel.Movies[1].Quantity = 1;

            var results = _controller.Index(ticketsModel, "Order") as RedirectResult;

            Assert.Equal("/home/order", results.Url);
        }

        [Fact]
        public void Index_UpdateButtonStateValid_ReturnView()
        {
            var ticketsModel = BuildTicketsModel();

            ticketsModel.Movies[1].Quantity = 1;

            var results = _controller.Index(ticketsModel, "Update") as ViewResult;
                        
            Assert.Equal("Index", results.ViewName);
        }

        [Fact]
        public void Index_StateNotValid_ReturnView()
        {
            var ticketsModel = BuildTicketsModel();            
           
            var results = _controller.Index(ticketsModel, "") as ViewResult;

            Assert.Equal("Index", results.ViewName);
        }

        private TicketsModel BuildTicketsModel()
        {
            var fakeMovies = new FakeMovies();

            var ticketsModel = new TicketsModel();

            ticketsModel.Movies = fakeMovies.LoadMovies();         

            return ticketsModel;
        }
    }         
}


