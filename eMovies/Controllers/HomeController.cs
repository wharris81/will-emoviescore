﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using eMovies.Models;
using eMovies.Repositories;
using eMovies.Services;

namespace eMovies.Controllers
{
    public class HomeController : Controller
    {
        private const string customerSessionKey = "customerdetails";
        private const string TicketsSessionKey = "ticketsModel";
        private const string CustomerIDSessionKey = "customerId";
        private readonly ISessionService _sessionService;
        private readonly IMovieRepository _movieRepository;

        public HomeController(ISessionService sessionService, IMovieRepository movieRepository)
        {
            _sessionService = sessionService;
            _movieRepository = movieRepository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var ticketsModel = new TicketsModel
            {
                Movies = _movieRepository.LoadMovies()
            };

            return View(ticketsModel);
        }

        [HttpGet]
        public IActionResult SessionTest()
        {
            _sessionService.SetString("key", "value");

            _sessionService.SetObject("key", new[] { "a", "b" });

            return View();
        }

        [HttpPost]
        public IActionResult Index(TicketsModel ticketsmodel, string submitbutton)
        {
            _movieRepository.LookupMovieDetails(ticketsmodel);

            _sessionService.SetObject(TicketsSessionKey, ticketsmodel);

            if (ModelState.IsValid)
            {
                if (submitbutton.Equals("Update"))
                {
                    return View("Index", ticketsmodel);
                }
                else if (submitbutton.Equals("Order"))
                {
                    int customerId = _movieRepository.NewCustomer(ticketsmodel);

                    _sessionService.SetObject(CustomerIDSessionKey, customerId);

                    return Redirect("/home/order");
                }
            }
            return View("Index", ticketsmodel);
        }

        [HttpGet]
        public IActionResult Order()
        {
            var ticketsmodel = _sessionService.GetObject<TicketsModel>(TicketsSessionKey);
            if (ticketsmodel == null)
            {
                return Redirect("/home");
            }
            return View();
        }

        [HttpPost]
        public IActionResult Order(CustomerDetails customerDetails)
        {
            if (ModelState.IsValid)
            {
                customerDetails.CustomerID = _sessionService.GetObject<int>(CustomerIDSessionKey);

                _movieRepository.SaveCustomerDetails(customerDetails);

                _sessionService.SetObject(customerSessionKey, customerDetails);

                return Redirect("/home/orderconf");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult OrderConf()
        {
            var customerdetailscheck = _sessionService.GetObject<CustomerDetails>(customerSessionKey);

            if ((customerdetailscheck) == null)
            {
                return Redirect("/home");
            }

            var customerdetails = _movieRepository.GetCustomerInfo(_sessionService.GetObject<int>(CustomerIDSessionKey));
            var ticketsmodel = _movieRepository.GetOrderInfo(_sessionService.GetObject<int>(CustomerIDSessionKey), _sessionService.GetObject<TicketsModel>(TicketsSessionKey));

            SummaryModel summarymodel = new SummaryModel
            {
                Customer = customerdetails,
                Tickets = ticketsmodel
            };

            return View(summarymodel);
        }

        [HttpGet]
        public IActionResult MovieDescription([FromQuery]int id)
        {
            if (id == 0)
            {
                return Redirect("/home");
            }

            var allMovies = _movieRepository.LoadMovies();

            var matchingMovie = allMovies.FirstOrDefault(a => a.Id == id);

            return View(matchingMovie);
        }
    }
}