﻿using System;
using System.Collections.Generic;

namespace eMovies.Comparers
{
    public class DoubleComparer : IEqualityComparer<double>
    {
        private int _decimalPlaces;

        public DoubleComparer(int decimalPlaces)
        {
            _decimalPlaces = decimalPlaces;
        }

        public bool Equals(double x, double y)
        {
            return Math.Round(y, _decimalPlaces) == Math.Round(x, _decimalPlaces);            
        }

        public int GetHashCode(double obj)
        {
            return obj.GetHashCode();
        }
    }    
}
