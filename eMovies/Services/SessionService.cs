﻿using eMovies.Extensions;
using Microsoft.AspNetCore.Http;

namespace eMovies.Services
{
    public class SessionService : ISessionService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SessionService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public virtual void SetString(string key, string value)
        {
            _httpContextAccessor.HttpContext.Session.SetObject(key, value);
        }

        public virtual string GetString(string key)
        {
            return _httpContextAccessor.HttpContext.Session.GetObject<string>(key);
        }

        public virtual void SetObject(string key, object value)
        {
            _httpContextAccessor.HttpContext.Session.SetObject(key, value);
        }

        public virtual T GetObject<T>(string key)
        {
            return _httpContextAccessor.HttpContext.Session.GetObject<T>(key);
        }
    }
}
